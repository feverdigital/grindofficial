(function ($) {

$(document).ready(function(){
$('h2.comment-form').after('<p class="readMore">Read Less</p>')
 
     $('h2.comment-form').click(function(){
               $(this).prev().slideToggle(function() {
                   $(this).next('h2.comment-form').text(function (index, text) {
                        return (text == 'Add new comment here' ? 'Add new comment' : 'Add new comment here');
                   });
               });
 
               return false;
    });
 
});

})(jQuery);