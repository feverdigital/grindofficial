<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */
 
 function grindxl_form_alter(&$form, &$form_state, $form_id) {
  if ($form['#id'] == 'user-login-form') {
    $form['name']['#title'] = t(''); //wrap any text in a t function
    $form['name']['#default_value'] = t('Username');
    $form['name']['#attributes']['onblur'] = "if (this.value == '') {this.value = 'Username';}";
    $form['name']['#attributes']['onfocus'] = "if (this.value == 'Username') {this.value = '';}";
    $form['name']['#required'] = FALSE;
    
    $form['pass']['#title'] = t('');
    $form['pass']['#attributes']['placeholder'] = t('Password');
    $form['pass']['#attributes']['onblur'] = "if (this.value == '') {this.value = 'Password';}";
    $form['pass']['#attributes']['onfocus'] = "if (this.value == 'Password') {this.value = '';}";
    $form['pass']['#required'] = FALSE;
    unset($form['links']); //remove links under fields
  }
  if ($form_id == 'search_block_form') {
    // HTML5 placeholder attribute
    $form['search_block_form']['#default_value'] = t('Search');
    //$form['search_block_form']['#attributes']['placeholder'] = t('enter search terms');
    $form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = 'Search';}";
    $form['search_block_form']['#attributes']['onfocus'] = "if (this.value == 'Search') {this.value = '';}";
  }
  if ($form_id == 'simplenews-block-form-5') {
    // HTML5 placeholder attribute
    $form['mail']['#attributes']['placeholder'] = t('Your Email');
    $form['mail']['#attributes']['onblur'] = "if (this.value == '') {this.value = 'Your Email';}";
    $form['mail']['#attributes']['onfocus'] = "if (this.value == 'Your Email') {this.value = '';}";
  }
}

/* Hides User picture from profile page */

function grindxl_preprocess_user_profile(&$variables) {
  unset($variables['user_profile']['user_picture']);
}

/* remove read more link */

function grindxl_preprocess_node(&$variables) {
  unset($variables['content']['links']['node']);
}

// Adds specific classes to primary tabs  
/**
 * Implements hook_menu_local_task().
 */
function grindxl_menu_local_task($vars) {
  $link = $vars['element']['#link'];
  $link_text = $link['title'];
  $id = preg_replace("/[^a-zA-Z0-9]/", "", strtolower(strip_tags($link_text))); // Converts the title to lowercase and removes extra spaces
  if (!empty($vars['element']['#active'])) {
    // Add text to indicate active tab for non-visual users.
    $active = '<span class="element-invisible">' . t('(active tab)') . '</span>';

    // If the link does not contain HTML already, check_plain() it now.
    // After we set 'html'=TRUE the link will not be sanitized by l().
    if (empty($link['localized_options']['html'])) {
      $link['title'] = check_plain($link['title']);
    }
    $link['localized_options']['html'] = TRUE;
    $link_text = t('!local-task-title!active', array('!local-task-title' => $link['title'], '!active' => $active));
  }
  return '<li' . (!empty($vars['element']['#active']) ? ' class="'.$id.' active"' : ' class="'.$id.'"') . '>' . l($link_text, $link['href'], $link['localized_options'], array('html' => TRUE)) . "</li>";
}